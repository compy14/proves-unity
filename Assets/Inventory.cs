﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Inventory : MonoBehaviour {
	public int pestanyes  = 4;
	public int fileres = 4;
	public int columnes = 5;
	public int posicioxinventori = 400;
	public int posicioyinventori = 400;
	public int tamanyslot = 100;
	public Texture slot;
	public Texture slotactiu;
	public Texture uiinventory;
	public Texture cursor;
	public ArrayList slotimatges;
	public GUITexture slotGUI;
	public GUISkin skin;
	public float difference = 1;
	public float differencey = 2f;
	public ArrayList slots = new ArrayList();
	public bool inventori = false;
	public GUIStyle myGUIStyle;
	public Font MyFont;

	private Vector3 mousePosition;
	private int ivalor;
	private float fvalor;
	private int posicioslot;
	private int xprova=0;
	private int xcursor;
	private int ycursor;
	private int yposiciocursor = 0;
	private int xposiciocursor = 0;
	private bool canpress = true;
	private int posicio = 0;
	private bool assignacio = false;
	private bool eliminar = false;
	private bool opciono = true;
	private bool opciosi = false;
	private bool descripcio = false;
	private int valorpestanya=0;
	private int multiplicador=0;
	private int cantitateliminar = 1;
	public bool objecteagafat = false;
	private int posicioagafat = 0;
	
	public static int xfinal = 275;
	public static int yfinal = 0;
	public static int widthpestanya  = 75;
	public static int widthslot = 90;
	public static int heightslot = 90;
	public class PestanyaPrincipal
	{
		public int x;
		public int y;
		public string nom;
		public bool activa;
		
		public PestanyaPrincipal(int xpestanya, int ypestanya, string nompestanya, bool activada)
		{
			x=xpestanya;
			y=ypestanya;
			nom=nompestanya;
			activa = activada;
		}
		
	}
	[System.Serializable]
	public class Slot
	{
		public int x;
		public int y;
		public string nomobjecte;
		public string funcioobjecte;
		public string imatge;
		public int parametre1;
		public int parametre2;
		public int quantitat;
		public string tipusobjecte;
		public string descripcio;
		public int quantitatmax;
		public bool ple;
		public bool equipat;
		public bool cursor;
		
		public Slot(int x2, int y2, string nomobjecte2, string funcioobjecte2, string imatge2, int parametre12, int parametre22, int quantitat2, string tipusobjecte2, string descripcioobjecte, int quantitatmax2, bool ple2, bool objecteequipat, bool cursor2)
		{
			x = x2;
			y = y2;;
			nomobjecte = nomobjecte2;
			funcioobjecte = funcioobjecte2;
			imatge = imatge2;
			parametre1 = parametre12;
			parametre2 = parametre22;
			quantitat = quantitat2;
			tipusobjecte = tipusobjecte2;
			quantitatmax = quantitatmax2;
			ple = ple2;
			equipat = objecteequipat;
			descripcio=descripcioobjecte;
			cursor2 = cursor;
		}

	}
	public List<PestanyaPrincipal> Pestanyes = new List<PestanyaPrincipal>();
	public List<Slot> Slots = new List<Slot>();
	private Inventory inv;
	private PestanyaPrincipal pestanya = new PestanyaPrincipal(xfinal,yfinal,"Consumibles", true);
	private PestanyaPrincipal pestanya2 = new PestanyaPrincipal(xfinal+widthpestanya,yfinal+widthpestanya,"Claus", false);
	private PestanyaPrincipal pestanya3 = new PestanyaPrincipal(xfinal+(widthpestanya*2),yfinal+(widthpestanya*2),"Pergamins", false);
	private PestanyaPrincipal pestanya4 = new PestanyaPrincipal(xfinal+(widthpestanya*3),yfinal+(widthpestanya*3),"Altres", false);
	// Use this for initialization
	void Start () {
		Pestanyes.Add(pestanya);
		Pestanyes.Add(pestanya2);
		Pestanyes.Add(pestanya3);
		Pestanyes.Add(pestanya4);
		crearslots ();
		xcursor = posicioxinventori-20;
		ycursor = posicioyinventori+30;
		//afegirobjecte("high potion", "curacio", "Assets/Resources/Objectes/Vida.png", 150, 0, "Consumibles");
	}
	
	// Update is called once per frame
	void Update () {
		mousePosition = Input.mousePosition;
		//Debug.Log (Slots[1].x*difference);
		for (int i = 0+valorpestanya; i < (fileres*columnes)+valorpestanya; i++) {
			//GUI.Box (new Rect(Slots[i].x*difference,Slots[i].y*differencey,widthslot*difference,heightslot*differencey), "", skin.GetStyle("Slots"));
			/*if(mousePosition.x > Slots[i].x && mousePosition.x < Slots[i].x+100 && Screen.height-mousePosition.y > Slots[i].y && Screen.height-mousePosition.y < Slots[i].y+100){
			
			}*/
			if(mousePosition.x > (Slots[i].x)*difference && mousePosition.x < (Slots[i].x+100)*difference && Screen.height - mousePosition.y > (Slots[i].y)*difference && Screen.height - mousePosition.y < (Slots[i].y+100)*difference ){
				//Debug.Log (mousePosition.x);
				Slots[i].cursor = true;
			}
			else Slots[i].cursor = false;

		}

		if(Input.GetButtonDown("Fire1")){
			for (int i = 0+valorpestanya; i < (fileres*columnes)+valorpestanya; i++) {
				if(Slots[i].cursor){
					descripcio = true;//////////////////////
					//////////////////////////////
					/// /////////////////////////////////////////////////////////
					/// Aqui ficarem lo de fer servir el objecte o whatever
				}
			}
		}

		if(!inventori){
			assignacio = false;
			descripcio=false;
		}
		difference = (Screen.width / 12.8f) /100;
		differencey = (Screen.height / 7.2f) /100;
	//	if(Input.GetButtonUp("Quadrat")){
		if(Input.GetKeyDown(KeyCode.I)){
			Pestanyes[0].activa=true;
			Pestanyes[1].activa=false;
			Pestanyes[2].activa=false;
			Pestanyes[3].activa=false;
			valorpestanya = 0;
			multiplicador=0;
			inventori = !inventori;
			if(inventori){
				xcursor = posicioxinventori-20;
				ycursor = posicioyinventori+30;
				xposiciocursor=0;
				yposiciocursor=0;
				
			}
		}
		if(inventori){
			///canviar de pestanyes
			if(!assignacio && !descripcio  && !eliminar){
				//if(Input.GetButtonUp("Fire")){
				if(Input.GetKeyDown(KeyCode.E)){
					Debug.Log ("entra en aquesta merda");
					Pestanyes[multiplicador].activa = false;
					multiplicador++;
					if(multiplicador>=pestanyes){
						multiplicador = pestanyes-1;
					}
					
					Pestanyes[multiplicador].activa = true;
					valorpestanya = multiplicador * ((fileres*columnes));
				}
				//else if(Input.GetButtonUp("Transformacio")){
				else if(Input.GetKeyDown(KeyCode.Q)){
					Pestanyes[multiplicador].activa = false;
					multiplicador--;
					if(multiplicador<0){
						multiplicador = 0;
					}
					valorpestanya = multiplicador * ((fileres*columnes));
					Pestanyes[multiplicador].activa = true;
				}
			}
			////descripcio
			if(!eliminar && !assignacio){
				posicioslot = yposiciocursor*columnes + xposiciocursor + valorpestanya;
				if(Input.GetKeyDown(KeyCode.R) && Slots[posicioslot].ple){
					if(!descripcio){
						StartCoroutine(cooldowndescripcio());
						posicioslot = yposiciocursor*columnes + xposiciocursor + valorpestanya;
					}
				}
			}
			///ho he fet aixi per si s'ha d'afegir alguna cosa
			if(descripcio){
				if(Input.GetKeyDown(KeyCode.R)){
					descripcio = false;
					xcursor = posicioxinventori-20;
					ycursor = posicioyinventori+30;
					xposiciocursor=0;
					yposiciocursor=0;
				}

			}
			////eliminar
			if(Input.GetKeyDown(KeyCode.Backspace) && !descripcio && !assignacio){
				posicioslot = yposiciocursor*columnes + xposiciocursor + valorpestanya;
				if(!eliminar && Slots[posicioslot].ple){
					cantitateliminar=1;
					eliminar = true;
					xcursor = posicioxinventori + 200;
					ycursor = posicioyinventori + 200;
				}

			}
			/*if(eliminar){
				posicioslot = yposiciocursor*columnes + xposiciocursor + valorpestanya;
				if(Input.GetKeyDown(KeyCode.D) && opciono){//dreta
				//	opciosi = true;
				//	opciono=false;
					xcursor = xcursor + 75;
				}
				else if(Input.GetKeyDown(KeyCode.A) && opciosi){//esquerra
				//	opciono = true;
				//	opciosi = false;
					xcursor = xcursor - 75;
				}
				if(Input.GetKeyDown(KeyCode.A) && canpress){
					//canpress=false;
					cantitateliminar--;
					if(cantitateliminar<0)cantitateliminar=0;
				}
				else if(Input.GetAxisRaw("Adalt")>0.9f && canpress){
					//canpress=false;
					cantitateliminar++;
					if(cantitateliminar>Slots[posicioslot].quantitat)cantitateliminar=Slots[posicioslot].quantitat;
				}
				/*else if(Input.GetAxisRaw("Adalt")==0){
					canpress=true;
				}*/
			/*	if(Input.GetButtonUp("Jump")){
					if(opciono){
						StartCoroutine(cooldowneliminar());
						xcursor = posicioxinventori-20;
						ycursor = posicioyinventori+30;
						xposiciocursor=0;
						yposiciocursor=0;
						opciono = true;
						opciosi = false;
					}
					else if (opciosi){
						if(cantitateliminar<Slots[posicioslot].quantitat){
							StartCoroutine(cooldowneliminar());
							posicioslot = yposiciocursor*columnes + xposiciocursor + valorpestanya;
							xcursor = posicioxinventori-20;
							ycursor = posicioyinventori+30;
							xposiciocursor=0;
							yposiciocursor=0;
							opciono = true;
							opciosi = false;
							Slots[posicioslot].quantitat=Slots[posicioslot].quantitat-cantitateliminar;
							
						}
						else if(cantitateliminar>=Slots[posicioslot].quantitat){
							StartCoroutine(cooldowneliminar());
							posicioslot = yposiciocursor*columnes + xposiciocursor + valorpestanya;
							xcursor = posicioxinventori-20;
							ycursor = posicioyinventori+30;
							xposiciocursor=0;
							yposiciocursor=0;
							opciono = true;
							opciosi = false;
							removeslots(posicioslot);
							///funcio per desequipar 
							//inv.remove(posicioslot);
							
						}
					}
					
				}
			}*/

			///////moviment amb tecles 
			/*if(canpress && !eliminar && !descripcio && !assignacio){
				if(Input.GetKeyDown(KeyCode.S) && yposiciocursor<fileres-1){
					ycursor=ycursor + tamanyslot;
					yposiciocursor++;
					canpress=false;
					StartCoroutine (cooldownpress());
					
				}
				else if(Input.GetKeyDown(KeyCode.W) && yposiciocursor>0){
					ycursor=ycursor - tamanyslot;
					yposiciocursor--;
					canpress=false;
					StartCoroutine (cooldownpress());
				}
				if(Input.GetKeyDown(KeyCode.D) && xposiciocursor<columnes-1){
					xcursor=xcursor + tamanyslot;
					xposiciocursor++;
					canpress=false;
					StartCoroutine (cooldownpress());
				}
				else if(Input.GetKeyDown(KeyCode.A) && xposiciocursor>0){
					xcursor=xcursor - tamanyslot;
					xposiciocursor--;
					canpress=false;
					StartCoroutine (cooldownpress());
				}
			}*/
		}
	}
	//Afegim tots els slots que hi ha en el inventori, de totes les pestanyes, despres ja filtrare a l'hora de pintar
	public void crearslots(){
		
		string nompestanya = "Consumibles";
		for (int i = 0; i < pestanyes; i++){
			if(i==1)nompestanya="Claus";
			if(i==2)nompestanya="Pergamins";
			if(i==3)nompestanya="Altres";
			for (int j = 0; j < fileres; j++){
				for (int k = 0; k < columnes; k++){
					
					Slot slot = new Slot(posicioxinventori + tamanyslot*k,posicioyinventori +  tamanyslot*j, "", "", "", 0, 0, 0, nompestanya,"",10, false, false, false);
					Slots.Add (slot);
					
				}
				
				
			}
		}

		
		
	}
	//funcio per afegir objectes, primer comprova que no n'hi hagi del mateix tipus i la quantitat maxima i despres afegeix si fa falta
	public void afegirobjecte(string nomobjecte, string funcio, string imatge, int parametre, int parametre2, string tipusobjecte, string descripcio){
		
		///for que serveix per veure si ja hi es
		
		for (int i = 0; i < Slots.Count; i++){
			
			if(Slots[i].nomobjecte == nomobjecte && Slots[i].tipusobjecte == tipusobjecte){
				
				if(Slots[i].quantitat < Slots[i].quantitatmax){
					Slots[i].quantitat ++;
					posicioagafat = i;
					StartCoroutine (pintarobjecte());
					return;
				}
			}
			
			
		}
		///for per objectes que no n'hi han
		
		for (int i = 0; i < Slots.Count; i++){
			if(Slots[i].ple== false && Slots[i].tipusobjecte == tipusobjecte){
				
				Slots[i].nomobjecte = nomobjecte;
				Slots[i].funcioobjecte = funcio;
				Slots[i].imatge = imatge;
				Slots[i].parametre1 = parametre;
				Slots[i].parametre2 = parametre2;
				Slots[i].quantitat++;
				Slots[i].descripcio = descripcio;
				Slots[i].ple = true;
				posicioagafat = i;
				StartCoroutine (pintarobjecte());
				return;
			}
		}
		
		
		
	}
	
	private IEnumerator pintarobjecte(){
		objecteagafat = true;
		yield return new WaitForSeconds (2.5f);
		objecteagafat = false;
		yield break;
		
	}
	//proves de pintar l'slot ho deixo per si fa falta
	void pintarslots(){
		
		for (int i = 0; i <5; i ++) { 
			Transform newSlot = ((GameObject)Instantiate(slotGUI.gameObject,this.transform.position,Quaternion.identity)).transform; // Creates a new heart
			newSlot.parent = transform;
			newSlot.GetComponent<GUITexture>().pixelInset = new Rect (100+xprova, 100, 75, 75);
			newSlot.GetComponent<GUITexture>().texture = slot;
		}
		
	}
	
	void OnGUI(){
		if(inventori){
			///pintem el fons
			//GUI.DrawTexture(new Rect((posicioxinventori-100)*difference,(posicioyinventori-150)*difference, columnes*130*difference , fileres * 150*difference), slot);
			GUI.DrawTexture(new Rect((posicioxinventori-250)*difference,(posicioyinventori-45)*differencey, 755*difference , 615*differencey), slot);
			//pintem els slots buits
			if(!descripcio){
				GUI.skin = skin;
				for (int i = 0+valorpestanya; i < (fileres*columnes)+valorpestanya; i++) {
					if(mousePosition.x > (Slots[i].x)*difference && mousePosition.x < (Slots[i].x+100)*difference && Screen.height - mousePosition.y > (Slots[i].y)*difference && Screen.height - mousePosition.y < (Slots[i].y+100)*difference ){
						//Debug.Log (mousePosition.x);
					}
					else GUI.Box (new Rect(Slots[i].x*difference,Slots[i].y*differencey,widthslot*difference,heightslot*differencey), "", skin.GetStyle("Slots"));
					
				}
				///pintem si hi ha alguna cosa a dins
				for (int i = 0+valorpestanya; i < (fileres*columnes)+valorpestanya; i++) {
					if(Slots[i].ple){
						Texture2D imatge;
						imatge = Resources.Load(Slots[i].imatge, typeof(Texture2D)) as Texture2D;
						GUI.DrawTexture(new Rect(Slots[i].x*difference,Slots[i].y*differencey, tamanyslot , tamanyslot), imatge);
						///pintem la quantitat dels objectes en questio
						//GUI.Label (new Rect(Slots[i].x*difference,Slots[i].y*difference, 25 , 25), Slots[i].quantitat);
						if(Slots[i].quantitat>1){
							fvalor = 24 * difference;
							ivalor = Mathf.FloorToInt (fvalor);
							myGUIStyle.fontSize = ivalor;
							myGUIStyle.normal.textColor = Color.white;
							GUI.Label (new   Rect(Slots[i].x*difference,Slots[i].y*difference, 30 , 30), "  X " + (int)Slots[i].quantitat, myGUIStyle);
						}
					}
				}
				////pintem les instruccions 
				/*fvalor = 24 * difference;
				ivalor = Mathf.FloorToInt (fvalor);
				myGUIStyle.fontSize = ivalor;
				myGUIStyle.normal.textColor = Color.white;
				Texture2D imatge2;
				imatge2 = Resources.Load("Objectes/botox", typeof(Texture2D)) as Texture2D;
				GUI.DrawTexture(new Rect((posicioxinventori-25) * difference,  (posicioyinventori+360) * difference, 25 , 25), imatge2);
				GUI.Label (new  Rect((posicioxinventori) * difference,  (posicioyinventori+363) * difference, 25 , 25), " Assignar ", myGUIStyle);
				imatge2 = Resources.Load("Objectes/botoquadrat", typeof(Texture2D)) as Texture2D;
				GUI.DrawTexture(new Rect((posicioxinventori+100) * difference,  (posicioyinventori+360) * difference, 25 , 25), imatge2);
				GUI.Label (new  Rect((posicioxinventori+125) * difference,  (posicioyinventori+363) * difference, 25 , 25), " Sortir ", myGUIStyle);
				imatge2 = Resources.Load("Objectes/botorodona", typeof(Texture2D)) as Texture2D;
				GUI.DrawTexture(new Rect((posicioxinventori+225) * difference,  (posicioyinventori+360) * difference, 25 , 25), imatge2);
				GUI.Label (new  Rect((posicioxinventori+250) * difference,  (posicioyinventori+363) * difference, 25 , 25), " Esborrar ", myGUIStyle);
				imatge2 = Resources.Load("Objectes/bototriangle", typeof(Texture2D)) as Texture2D;
				GUI.DrawTexture(new Rect((posicioxinventori+350) * difference,  (posicioyinventori+360) * difference, 25 , 25), imatge2);
				GUI.Label (new  Rect((posicioxinventori+375) * difference,  (posicioyinventori+363) * difference, 25 , 25), " Descripcio ", myGUIStyle);
				GUI.Label (new  Rect((posicioxinventori+150) * difference,  (posicioyinventori+390) * difference, 25 , 25), " R1-L1 canvi pestanya ", myGUIStyle);*/
			}
			/// pintem quan hem d'assignar els botons
			if(assignacio){
				
				GUI.DrawTexture (new Rect ((posicioxinventori+200) * difference, (posicioyinventori+100) * difference, 150 * difference, 150 * difference), uiinventory, ScaleMode.ScaleToFit, true);
				fvalor = 40 * difference;
				ivalor = Mathf.FloorToInt (fvalor);
				myGUIStyle.fontSize = ivalor;
				myGUIStyle.normal.textColor = Color.white;
				GUI.Label (new   Rect((posicioxinventori+200) * difference,  (posicioyinventori+300) * difference, 150 , 150), "  Press Button ", myGUIStyle);
				
			}
			if(eliminar){
				//NO
				fvalor = 40 * difference;
				ivalor = Mathf.FloorToInt (fvalor);
				myGUIStyle.fontSize = ivalor;
				myGUIStyle.normal.textColor = Color.white;
				GUI.Label (new   Rect((posicioxinventori+200) * difference,  (posicioyinventori+150) * difference, 150 , 150), "  Are you sure? ", myGUIStyle);
				GUI.Label (new   Rect((posicioxinventori+225) * difference,  (posicioyinventori+190) * difference, 150 , 150), "  No ", myGUIStyle);
				GUI.Label (new   Rect((posicioxinventori+300) * difference,  (posicioyinventori+190) * difference, 150 , 150), "  Si ", myGUIStyle);
				GUI.Label (new   Rect((posicioxinventori+300) * difference,  (posicioyinventori+100) * difference, 150 , 150), "" + (int)cantitateliminar, myGUIStyle);
				Texture2D imatge;
				imatge = Resources.Load("Objectes/adalt", typeof(Texture2D)) as Texture2D;
				GUI.DrawTexture(new Rect((posicioxinventori+250) * difference,  (posicioyinventori+65) * difference, 30 , 50), imatge);
				imatge = Resources.Load("Objectes/abaix", typeof(Texture2D)) as Texture2D;
				GUI.DrawTexture(new Rect((posicioxinventori+250) * difference,  (posicioyinventori+110) * difference, 30 , 50), imatge);
				
			}
			if(descripcio){
				Texture2D imatge;
				imatge = Resources.Load(Slots[posicioslot].imatge, typeof(Texture2D)) as Texture2D;
				GUI.DrawTexture(new Rect(posicioxinventori-40*difference,posicioyinventori*differencey, 150 , 150), imatge);
				fvalor = 40 * difference;
				ivalor = Mathf.FloorToInt (fvalor);
				GUIStyle centeredTextStyle = new GUIStyle("label");
				centeredTextStyle.normal.textColor= Color.white;
				centeredTextStyle.alignment = TextAnchor.LowerCenter;
				centeredTextStyle.fontSize= ivalor;
				GUILayout.BeginArea(new  Rect(posicioxinventori*difference,posicioyinventori+70*difference, 520 , 600));
				GUILayout.FlexibleSpace();
				GUILayout.Label (Slots[posicioslot].descripcio, centeredTextStyle);
				GUILayout.FlexibleSpace();
				GUILayout.EndArea ();
				fvalor = 80 * difference;
				ivalor = Mathf.FloorToInt (fvalor);
				myGUIStyle.fontSize = ivalor;
				myGUIStyle.normal.textColor = Color.white;
				GUI.Label (new   Rect(posicioxinventori*difference+150,posicioyinventori*difference, 150 , 150), Slots[posicioslot].nomobjecte, myGUIStyle);
				fvalor = 60 * difference;
				ivalor = Mathf.FloorToInt (fvalor);
				myGUIStyle.fontSize = ivalor;
				myGUIStyle.normal.textColor = Color.white;
				GUI.Label (new   Rect(posicioxinventori*difference+150,posicioyinventori*difference+70, 150 , 150), Slots[posicioslot].tipusobjecte, myGUIStyle);
			}
		
			/////pintem les pestanyes
			if(!descripcio){
				for (int i = 0; i < pestanyes; i++) {
					Texture2D imatge;
					if(Pestanyes[i].activa){
						imatge = Resources.Load("Objectes/slotactiu", typeof(Texture2D)) as Texture2D;
					}
					else{
						imatge = Resources.Load("Objectes/slot", typeof(Texture2D)) as Texture2D;
					}
					GUI.DrawTexture(new Rect((posicioxinventori-215)*difference,(300+Pestanyes[i].y)*differencey, 165*difference ,73*differencey), imatge);
					fvalor = 40 * difference;
					ivalor = Mathf.FloorToInt (fvalor);
					GUIStyle stylehot = new GUIStyle("label");
					stylehot.font = MyFont;
					stylehot.fontSize = ivalor;
					stylehot.normal.textColor = Color.white;
					stylehot.alignment = TextAnchor.MiddleCenter;
					GUI.Label (new Rect((posicioxinventori-217)*difference,(300+Pestanyes[i].y)*differencey, 165*difference ,73*differencey), Pestanyes[i].nom, stylehot);
				}
			}
		}
		
		if(objecteagafat){
			
			Texture2D imatge;
			imatge = Resources.Load(Slots[posicioagafat].imatge, typeof(Texture2D)) as Texture2D;
			GUI.DrawTexture(new Rect(440 * difference,  600 * difference, 75*difference , 75*difference), imatge);
			fvalor = 65 * difference;
			ivalor = Mathf.FloorToInt (fvalor);
			GUIStyle TextStyle = new GUIStyle("label");
			TextStyle.fontSize = ivalor;
			TextStyle.alignment = TextAnchor.MiddleCenter;
			TextStyle.normal.textColor = Color.white;
			GUI.Label (new   Rect(540 * difference,  600 * difference, 250*difference , 75*difference), Slots[posicioagafat].nomobjecte, TextStyle);
		}
	}
	public void removeslots(int posicio){
		
		Slots[posicio].nomobjecte = "";
		Slots[posicio].funcioobjecte = "";
		Slots[posicio].imatge = "";
		Slots[posicio].parametre1 = 0;;
		Slots[posicio].parametre2 = 0;;
		Slots[posicio].quantitat=0;
		Slots[posicio].ple = false;
		Slots[posicio].equipat = false;
		
	}
	//un cooldown per a que no esvegi tant seguit, si eso ajustar el temps d'espera, crec que esta be
	private IEnumerator cooldownpress(){
		
		yield return new WaitForSeconds(0.1f);
		canpress = true;
		yield break;
	}
	
	private IEnumerator cooldowneliminar(){
		yield return new WaitForSeconds(0.1f);
		eliminar = false;
		yield break;
		
	}
	
	private IEnumerator cooldowndescripcio(){
		yield return new WaitForSeconds(0.1f);
		descripcio = !descripcio;
		yield break;
		
	}
}
