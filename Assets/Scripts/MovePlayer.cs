﻿using UnityEngine;
using System.Collections;

public class MovePlayer : MonoBehaviour {
	public bool click = false;
	public GameObject target;

	private float Speed = 3;
	private PathDefinition pt;
	public int PartPlayer = 1;
	private int PartClick;
	private Click cl;
	private bool once_instance_click = true;
	// Use this for initialization
	void Start () {
		pt = GameObject.Find("Path").GetComponent<PathDefinition>();
	}
	
	// Update is called once per frame
	void Update () {
		if(click){
			if (GameObject.Find ("click(Clone)") != null) {
				cl = GameObject.Find("click(Clone)").GetComponent<Click>();
			}
			target = cl.gameObject;
			PartClick = cl.Part;

			if(PartPlayer == PartClick){
				transform.position = Vector3.MoveTowards(transform.position, cl.transform.position, Time.deltaTime * Speed);
				//move to click
			}
			else if(PartPlayer != PartClick && PartPlayer!=2 && PartClick!=2){
				transform.position = Vector3.MoveTowards(transform.position, pt.Points[1].transform.position, Time.deltaTime * Speed);
				if (Vector2.Distance(transform.position,pt.Points[1].transform.position)<0.1f ) {
					PartPlayer = 2;

				}

				///move to 2
			}
			else if (PartPlayer==2 || PartClick == 2){
				//move to click
				transform.position = Vector3.MoveTowards(transform.position, cl.transform.position, Time.deltaTime * Speed);
			}

			///calculem la distancia envers el click, per llavors frenar.
			if (Vector2.Distance(transform.position,target.transform.position)<0.1f ) {
				cl.destroy();
				click = false;
			}

		}
	}



	void OnTriggerStay2D(Collider2D other){
		if(other.gameObject.tag =="Part1"){
			PartPlayer = 1;

		}
		if(other.gameObject.tag =="Part2"){
			if(PartClick==2)PartPlayer = 2;
			
		}
		if(other.gameObject.tag =="Part3"){
			PartPlayer = 3;
			
		}

	}

	public void clicktrue(){
		click=true;
	}

	public void clickfalse(){

		click = false;
	}
}
