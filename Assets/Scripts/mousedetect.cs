﻿using UnityEngine;
using System.Collections;

public class mousedetect : MonoBehaviour {
	private Vector3 screenPoint;
	private Vector3 offset;
	private Click cl;
	private MovePlayer mp;
	private Inventory inv;
	public GameObject click;
	// Use this for initialization
	void Start () {
		inv = GameObject.Find("Inventory").GetComponent<Inventory>();
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnMouseDown()
	{
		if(!inv.inventori){
			///destruim qualsevol click anterior
			if (GameObject.Find ("click(Clone)") != null) {
				cl = GameObject.Find("click(Clone)").GetComponent<Click>();
				if(cl!=null){
					cl.destroy();

				}
			}
			///agafem posicio de la camara respecte escena
			offset =  Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
			//creem click
			Instantiate(click, new Vector3 (offset.x, offset.y, 0), Quaternion.identity);
			//activem la variable click en el player
			mp = GameObject.Find("Player").GetComponent<MovePlayer>();
			mp.SendMessage("clicktrue");

		}
	}
}
